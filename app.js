const express = require("express");
const app = express();

const port = 8000;

app.get('/', (req, res)=>{
    res.send("This is the first project on linux system")
})

app.listen(port, ()=>{
    console.log(`this app is running on port:${port}`)
})